"""basket_fdf URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include

from calculator import views

if settings.DEBUG:
    import debug_toolbar

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^potential_team/(?P<team>\w{0,50})/$', views.potential_team, name='potential'),
    url(r'^current_team/(?P<team>\w{0,50})/$', views.current_team, name='current'),
    url(r'^redistribute_team/(?P<team>\w{0,50})/$', views.redistributed_team, name='redistribution'),
    url(r'^match/(?P<team>\w{0,50})/$', views.current_team_match, name='current'),
    url(r'^market_team/', views.market_team, name='market_team'),
    url(r'^market/', views.market, name='market'),
    url(r'^roster_current/(?P<team_url>\w{0,50})/$', views.roster_current, name='roster_current'),
    url(r'^roster_potential/(?P<team_url>\w{0,50})/$', views.roster_potential, name='roster_potential'),
    url(r'^roster_editor/(?P<team_url>\w{0,50})/$', views.roster_editor, name='roster_editor'),
    url(r'^statistics/', views.statistics_summary, name='statistics'),
    url(r'^api/', include('calculator.api_urls')),
    path('__debug__/', include(debug_toolbar.urls)),
]
