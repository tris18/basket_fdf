jQuery(function($) {
    var $row = ""
    // using jQuery
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
    var csrftoken = getCookie('csrftoken');

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });

    function callbackCalculateShoot(style,points){
        $row.find(".style").text(style);
        $row.find(".points").text(points);
   }

    $(".calculate").click(function () {
        $row = $(this).closest("tr");
        var item = {};

        item["id"] = $(this).data("id")
        item["block"] = $row.find(".block").text()
        item["time"] = $row.find(".time").text()
        item["shoot"] = $row.find(".shoot").text()
        item["position"] = $row.find(".position").text()

        jsonObj = [];
        jsonObj.push(item);

        $.ajax({
            type: "GET",
            url: "/api/get_player_match_shoot",
            data: item,
            success: function(data) {
                callbackCalculateShoot(data["style"],data["total"]);
             }
        });
    });

});