jQuery(function($) {

    // using jQuery
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
    var csrftoken = getCookie('csrftoken');

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });

    $(".player").click(function () {
        var $row = $(this).closest("tr");
        var item = {};

        item["id"] = $(this).data("id")
        item["name"] = $row.find(".name").text();
        item["exp"] = $row.find(".exp").text();
        item["pos"] = $row.find(".pos").text();
        item["t1"] = $row.find(".t1").text();
        item["t2"] = $row.find(".t2").text();
        item["t3"] = $row.find(".t3").text();
        item["dunk"] = $row.find(".dunk").text();
        item["rebound"] = $row.find(".rebound").text();
        item["block"] = $row.find(".block").text();
        item["passing"] = $row.find(".passing").text();
        item["steal"] = $row.find(".steal").text();
        item["price"] = $row.find(".price").text();
        item["age"] = $row.find(".age").text();
        item["motivated"] = $row.find(".motivated").find("input").is(":checked")
        item["enabled"] = $row.find(".enabled").find("input").is(":checked")
        item["team"] = $row.find(".team").text();
        item["signed"] = $row.find(".signed").text();
        item["loaned"] = $row.find(".loaned").text();

        jsonObj = [];
        jsonObj.push(item);

        $.ajax({
            type: "POST",
            url: "/api/update_player",
            data: item,
            success: function() {
                $('.success_dialog').fadeIn(200).delay(1500).fadeOut(400);
                }
        });
    });

    $(".add_player").click(function () {
        var $row = $(this).closest("tr");
        var item = {};

        item["name"] = $row.find(".name").text();
        item["exp"] = $row.find(".exp").text();
        item["pos"] = $row.find(".pos").text();
        item["t1"] = $row.find(".t1").text();
        item["t2"] = $row.find(".t2").text();
        item["t3"] = $row.find(".t3").text();
        item["dunk"] = $row.find(".dunk").text();
        item["rebound"] = $row.find(".rebound").text();
        item["block"] = $row.find(".block").text();
        item["passing"] = $row.find(".passing").text();
        item["steal"] = $row.find(".steal").text();
        item["price"] = $row.find(".price").text();
        item["age"] = $row.find(".age").text();
        item["motivated"] = $row.find(".motivated").find("input").is(":checked")
        item["enabled"] = $row.find(".enabled").find("input").is(":checked")
        item["team"] = $row.find(".team").text();
        item["signed"] = $row.find(".signed").text();
        item["loaned"] = $row.find(".loaned").text();

        jsonObj = [];
        jsonObj.push(item);

        $.ajax({
            type: "POST",
            url: "/api/add_player",
            data: item,
            success: function() {
                $('.success_dialog').fadeIn(200).delay(1500).fadeOut(400);
                location.reload()
             }
        });
    });
});