from django.db.models import Q

from calculator.constants import TEAM_FREE_THROWS, TEAM_SHOTS, CENTER_BLOCK, CENTER_AVG_BLOCK, POWERFORWARD_AVG_BLOCK, \
    SMALLFORWARD_AVG_BLOCK, SHOOTINGGUARD_AVG_BLOCK, POINTGUARD_AVG_BLOCK, POINTGUARD_BLOCK, SHOOTINGGUARD_BLOCK, \
    SMALLFORWARD_BLOCK, POWERFORWARD_BLOCK, MY_TEAM, MIN_POINTS, MIN_MAX_POINTS_DIFFERENCE, POINTGUARD_BLOCK_RESERVE, \
    SHOOTINGGUARD_BLOCK_RESERVE, SMALLFORWARD_BLOCK_RESERVE, POWERFORWARD_BLOCK_RESERVE, CENTER_BLOCK_RESERVE
from calculator.models import Player, Position, OffensiveConfiguration
from calculator.player_modificators import get_experience_handicap
from calculator.player_shoot import get_interior_shoot_percentage, get_two_point_shoot_percentage, \
    get_three_point_shoot_percentage


def calculate_skills(p: Player, current: bool) -> dict:
    if current:
        # aux = get_moral_handicap(p.moral, 'HOME') + get_experience_handicap(p.experience)
        aux = get_experience_handicap(p.experience)

        skills = {'onePointShoot': max(p.onePointShoot - aux, 1),
                  'twoPointShoot': max(p.twoPointShoot - aux, 1),
                  'threePointShoot': max(p.threePointShoot - aux, 1),
                  'dunk': max(p.dunk - aux, 1),
                  'rebound': max(p.rebound - aux, 1),
                  'block': max(p.block - aux, 1),
                  'passing': max(p.passing - aux, 1),
                  'steal': max(p.steal - aux, 1)}
    else:
        aux = 0 if p.motivated else 5

        skills = {'onePointShoot': p.onePointShoot + aux,
                  'twoPointShoot': p.twoPointShoot + aux,
                  'threePointShoot': p.threePointShoot + aux,
                  'dunk': p.dunk + aux,
                  'rebound': p.rebound + aux,
                  'block': p.block + aux,
                  'passing': p.passing + aux,
                  'steal': p.steal + aux}

    return skills


def get_style_rating(c: OffensiveConfiguration, p: Player, current: bool):
    skills = calculate_skills(p, current)
    aux = skills.get('dunk') * c.interiorRate / 100 + skills.get('twoPointShoot') * c.midRate / 100
    aux += skills.get('threePointShoot') * c.exteriorRate / 100
    aux = 0.15 * skills.get('onePointShoot') + 0.85 * aux
    return aux


def get_offensive_rating(player: Player, position: Position, current: bool):
    skills = calculate_skills(player, current)
    configurations = OffensiveConfiguration.objects.filter(position=position.code)

    total = 0
    style = ''
    styles_dict = {}
    for c in configurations:
        aux = skills.get('dunk') * c.interiorRate / 100 + skills.get('twoPointShoot') * c.midRate / 100
        aux += skills.get('threePointShoot') * c.exteriorRate / 100
        aux = 0.15 * skills.get('onePointShoot') + aux * 0.85
        styles_dict.update({c.name: aux})

        if aux > total:
            total = aux
            style = c.name

    d = {
        'total': total,
        'style': style,
        'styles_dict': styles_dict
    }
    return d


def get_optimal_offensive_rating(shooter: Player, position: Position, current: bool, starting: bool = True,
                                 specific_match: bool = False):
    block = 0
    if position.code == 'B':
        if specific_match:
            if starting:
                block = POINTGUARD_BLOCK
            else:
                block = POINTGUARD_BLOCK_RESERVE
        else:
            block = POINTGUARD_AVG_BLOCK
    elif position.code == 'E':
        if specific_match:
            if starting:
                block = SHOOTINGGUARD_BLOCK
            else:
                block = SHOOTINGGUARD_BLOCK_RESERVE
        else:
            block = SHOOTINGGUARD_AVG_BLOCK
    elif position.code == 'A':
        if specific_match:
            if starting:
                block = SMALLFORWARD_BLOCK
            else:
                block = SMALLFORWARD_BLOCK_RESERVE
        else:
            block = SMALLFORWARD_AVG_BLOCK
    elif position.code == 'AP':
        if specific_match:
            if starting:
                block = POWERFORWARD_BLOCK
            else:
                block = POWERFORWARD_BLOCK_RESERVE
        else:
            block = POWERFORWARD_AVG_BLOCK
    elif position.code == 'P':
        if specific_match:
            if starting:
                block = CENTER_BLOCK
            else:
                block = CENTER_BLOCK_RESERVE
        else:
            block = CENTER_AVG_BLOCK

    time_percentage = 0.75 if starting else 0.25
    player_shoots = 0.2

    skills = calculate_skills(shooter, current)

    interior = get_interior_shoot_percentage(skills.get('dunk'), block)
    two_point = get_two_point_shoot_percentage(skills.get('twoPointShoot'), block)
    three_point = get_three_point_shoot_percentage(skills.get('threePointShoot'), block)

    number_shoots = TEAM_SHOTS * player_shoots
    number_free_throws = TEAM_FREE_THROWS * player_shoots

    total = 0
    result = {}
    for c in position.offensiveconfiguration_set.all():
        exterior = (c.exteriorRate / 100 * number_shoots * three_point) * 3
        mid = (c.midRate / 100 * number_shoots * two_point) * 2
        interior_points = (c.interiorRate / 100 * number_shoots * interior) * 2
        free_throw_points = skills.get('onePointShoot') / 100 * number_free_throws
        aux = exterior + mid + interior_points + free_throw_points
        aux_main = aux * 0.75
        rating = (aux_main - MIN_POINTS) / MIN_MAX_POINTS_DIFFERENCE * 100

        aux = aux * time_percentage
        if aux >= total:
            total = aux
            result = {'style': c.name, 'total': get_style_rating(c, shooter, current), 'points': aux,
                      'real_rating': rating}

    return result


def calculate_position(p: Player, pos: Position, current: bool, starting: bool = True, specific_match: bool = False,
                       evaluate: bool = True):
    skills = calculate_skills(p, current)
    total = skills.get('onePointShoot') * 0.01
    support = (pos.passingRate / 100 * skills.get('passing') + pos.reboundRate / 100 * skills.get('rebound'))
    defensive = (pos.stealRate / 100 * skills.get('steal') + pos.blockRate / 100 * skills.get('block'))
    total += (support + defensive) * 0.33

    offensive = get_optimal_offensive_rating(p, pos, current, starting, specific_match)

    total += (offensive.get('total')) * 0.33

    total = total if evaluate else 0
    return {'id': p.id, 'total': total, 'offensive': offensive.get('total'), 'style': offensive.get('style'),
            'points': offensive.get('points'),
            'team': p.team,
            'defensive': defensive, 'support': support, 'real_position': p.position.code, 'experience': p.experience,
            'real_rating': offensive.get('real_rating')}


def calculate_position_offensive(p: Player, pos: Position, current: bool, starting: bool = True,
                                 specific_match: bool = False, evaluate: bool = True):
    skills = calculate_skills(p, current)
    total = skills.get('onePointShoot') * 0.01
    support = (pos.passingRate / 100 * skills.get('passing') + pos.reboundRate / 100 * skills.get('rebound'))
    defensive = (pos.stealRate / 100 * skills.get('steal') + pos.blockRate / 100 * skills.get('block'))
    total += (support + defensive) * 0.4 / 2

    offensive = get_optimal_offensive_rating(p, pos, current, starting, specific_match)

    total += (offensive.get('total')) * 0.60

    total = total if evaluate else 0

    return {'id': p.id, 'total': total, 'offensive': offensive.get('total'), 'style': offensive.get('style') + ' - O',
            'points': offensive.get('points'),
            'team': p.team,
            'defensive': defensive, 'support': support, 'real_position': p.position.code, 'experience': p.experience}


def calculate_position_defensive(p: Player, pos: Position, current: bool, starting: bool = True,
                                 specific_match: bool = False, evaluate: bool = True):
    skills = calculate_skills(p, current)
    total = skills.get('onePointShoot') * 0.01
    support = (pos.passingRate / 100 * skills.get('passing') + pos.reboundRate / 100 * skills.get('rebound'))
    defensive = (pos.stealRate / 100 * skills.get('steal') + pos.blockRate / 100 * skills.get('block'))
    total += (support + defensive) * 0.495

    offensive = get_optimal_offensive_rating(p, pos, current, starting, specific_match)

    total = total if evaluate else 0

    return {'id': p.id, 'total': total, 'offensive': offensive.get('total'),
            'style': str(offensive.get('style')) + ' - D',
            'points': offensive.get('points'),
            'team': p.team,
            'defensive': defensive, 'support': support, 'real_position': p.position.code, 'experience': p.experience}


def get_all_positions(p: Player, current: bool):
    positions = Position.objects.all()
    for pos in positions:
        print(str(calculate_position(p, pos.code, current)) + ' -> ' + pos.code)


def get_players_ratings(players: set, current: bool, starting: bool = True, specific_match: bool = False,
                        redistribute: bool = False) -> dict:
    ret = {}

    positions = Position.objects.all().prefetch_related('offensiveconfiguration_set')
    for player in players:
        aux = {'name': player.name, 'team': player.team, 'id': player.id,
               'price': player.price if player.team != MY_TEAM and player.signed != MY_TEAM else 0,
               'passing': player.passing, 'steal': player.steal}
        for position in positions:

            evaluate = (not current) or (redistribute and player.experience > 75)

            if player.position.code in ('B', 'E') and position.code in ('B', 'E'):
                evaluate = True
            elif player.position.code in ('A', 'AP') and position.code in ('A', 'AP'):
                evaluate = True
            elif player.position.code == 'P' and position.code == 'P':
                evaluate = True

            avg = calculate_position(player, position, current, starting, specific_match, evaluate)
            off = calculate_position_offensive(player, position, current, starting, specific_match, evaluate)
            d = calculate_position_defensive(player, position, current, starting, specific_match, evaluate)

            aux.update({position.code: avg})
            aux.update({position.code + '_O': off})
            aux.update({position.code + '_D': d})

        ret.update({player.pk: aux})

    return ret


def get_players_ratings_center(players: set, current: bool, starting: bool = True,
                               specific_match: bool = False) -> dict:
    ret = {}

    for player in players:
        aux = {'name': player.name, 'team': player.team, 'id': player.id,
               'passing': player.passing, 'steal': player.steal}
        for position in Position.objects.filter(code='P'):
            aux.update({position.code: calculate_position(player, position.code, current, starting, specific_match),
                        'price': player.price})
            aux.update(
                {position.code + '_O': calculate_position_offensive(player, position.code, current, starting,
                                                                    specific_match),
                 'price': player.price})
            aux.update({position.code + '_D': calculate_position_defensive(player, position.code, current, starting,
                                                                           specific_match),
                        'price': player.price})
        ret.update({player.pk: aux})

    return ret


def get_players_ratings_forward(players: set, current: bool, starting: bool = True,
                                specific_match: bool = False) -> dict:
    ret = {}

    for player in players:
        aux = {'name': player.name, 'team': player.team, 'id': player.id,
               'passing': player.passing, 'steal': player.steal}
        for position in Position.objects.filter(Q(code='AP') | (Q(code='A'))):
            aux.update({position.code: calculate_position(player, position.code, current, starting, specific_match),
                        'price': player.price})
            aux.update(
                {position.code + '_O': calculate_position_offensive(player, position.code, current, starting,
                                                                    specific_match),
                 'price': player.price})
            aux.update({position.code + '_D': calculate_position_defensive(player, position.code, current, starting,
                                                                           specific_match),
                        'price': player.price})
        ret.update({player.pk: aux})

    return ret


def get_players_ratings_guards(players: set, current: bool, starting: bool = True,
                               specific_match: bool = False) -> dict:
    ret = {}

    for player in players:
        aux = {'name': player.name, 'team': player.team, 'id': player.id,
               'passing': player.passing, 'steal': player.steal}
        for position in Position.objects.filter(Q(code='B') | (Q(code='E'))):
            aux.update({position.code: calculate_position(player, position.code, current, starting, specific_match),
                        'price': player.price})
            aux.update(
                {position.code + '_O': calculate_position_offensive(player, position.code, current, starting,
                                                                    specific_match),
                 'price': player.price})
            aux.update({position.code + '_D': calculate_position_defensive(player, position.code, current, starting,
                                                                           specific_match),
                        'price': player.price})
        ret.update({player.pk: aux})

    return ret


def get_all_history_greatests(total_all_players) -> dict:
    max_games = 0
    max_games_player = ''
    max_assists = 0
    max_assists_player = ''
    max_rebounds = 0
    max_rebounds_player = ''
    max_defensive_rebounds = 0
    max_defensive_rebounds_player = ''
    max_offensive_rebounds = 0
    max_offensive_rebounds_player = ''
    max_steals = 0
    max_steals_player = ''
    max_blocks = 0
    max_blocks_player = ''
    max_points = 0
    max_points_player = ''

    for reg in total_all_players:
        if reg.get('games__sum') > max_games:
            max_games = reg.get('games__sum')
            max_games_player = reg.get('player__name')

        if reg.get('assists__sum') > max_assists:
            max_assists = reg.get('assists__sum')
            max_assists_player = reg.get('player__name')

        rebounds = reg.get('defensive_rebounds__sum') + reg.get('offensive_rebounds__sum')
        if rebounds > max_rebounds:
            max_rebounds = rebounds
            max_rebounds_player = reg.get('player__name')

        if reg.get('defensive_rebounds__sum') > max_defensive_rebounds:
            max_defensive_rebounds = reg.get('defensive_rebounds__sum')
            max_defensive_rebounds_player = reg.get('player__name')

        if reg.get('offensive_rebounds__sum') > max_offensive_rebounds:
            max_offensive_rebounds = reg.get('offensive_rebounds__sum')
            max_offensive_rebounds_player = reg.get('player__name')

        if reg.get('steals__sum') > max_steals:
            max_steals = reg.get('steals__sum')
            max_steals_player = reg.get('player__name')

        if reg.get('blocks__sum') > max_blocks:
            max_blocks = reg.get('blocks__sum')
            max_blocks_player = reg.get('player__name')

        if reg.get('points__sum') > max_points:
            max_points = reg.get('points__sum')
            max_points_player = reg.get('player__name')

    return {
        'max_games': max_games,
        'max_games_player': max_games_player,
        'max_assists': max_assists,
        'max_assists_player': max_assists_player,
        'max_rebounds': max_rebounds,
        'max_rebounds_player': max_rebounds_player,
        'max_defensive_rebounds': max_defensive_rebounds,
        'max_defensive_rebounds_player': max_defensive_rebounds_player,
        'max_offensive_rebounds': max_offensive_rebounds,
        'max_offensive_rebounds_player': max_offensive_rebounds_player,
        'max_steals': max_steals,
        'max_steals_player': max_steals_player,
        'max_blocks': max_blocks,
        'max_blocks_player': max_blocks_player,
        'max_points': max_points,
        'max_points_player': max_points_player,
    }


def get_all_history_greatests_seasons(total_all_players) -> dict:
    max_games = 0
    max_games_player = ''
    max_games_season = ''
    max_assists = 0
    max_assists_player = ''
    max_assists_season = ''
    max_rebounds = 0
    max_rebounds_player = ''
    max_rebounds_season = ''
    max_defensive_rebounds = 0
    max_defensive_rebounds_player = ''
    max_defensive_rebounds_season = ''
    max_offensive_rebounds = 0
    max_offensive_rebounds_player = ''
    max_offensive_rebounds_season = ''
    max_steals = 0
    max_steals_player = ''
    max_steals_season = ''
    max_blocks = 0
    max_blocks_player = ''
    max_blocks_season = ''
    max_points = 0
    max_points_player = ''
    max_points_season = ''

    for reg in total_all_players:
        if reg.get('games__sum') > max_games:
            max_games = reg.get('games__sum')
            max_games_player = reg.get('player__name')
            max_games_season = reg.get('season_id')

        if reg.get('assists__sum') > max_assists:
            max_assists = reg.get('assists__sum')
            max_assists_player = reg.get('player__name')
            max_assists_season = reg.get('season_id')

        rebounds = reg.get('defensive_rebounds__sum') + reg.get('offensive_rebounds__sum')
        if rebounds > max_rebounds:
            max_rebounds = rebounds
            max_rebounds_player = reg.get('player__name')
            max_rebounds_season = reg.get('season_id')
        if reg.get('defensive_rebounds__sum') > max_defensive_rebounds:
            max_defensive_rebounds = reg.get('defensive_rebounds__sum')
            max_defensive_rebounds_player = reg.get('player__name')
            max_defensive_rebounds_season = reg.get('season_id')
        if reg.get('offensive_rebounds__sum') > max_offensive_rebounds:
            max_offensive_rebounds = reg.get('offensive_rebounds__sum')
            max_offensive_rebounds_player = reg.get('player__name')
            max_offensive_rebounds_season = reg.get('season_id')
        if reg.get('steals__sum') > max_steals:
            max_steals = reg.get('steals__sum')
            max_steals_player = reg.get('player__name')
            max_steals_season = reg.get('season_id')

        if reg.get('blocks__sum') > max_blocks:
            max_blocks = reg.get('blocks__sum')
            max_blocks_player = reg.get('player__name')
            max_blocks_season = reg.get('season_id')
        if reg.get('points__sum') > max_points:
            max_points = reg.get('points__sum')
            max_points_player = reg.get('player__name')
            max_points_season = reg.get('season_id')
    return {
        'max_games': max_games,
        'max_games_player': max_games_player,
        'max_games_season': max_games_season,
        'max_assists': max_assists,
        'max_assists_player': max_assists_player,
        'max_assists_season': max_assists_season,
        'max_rebounds': max_rebounds,
        'max_rebounds_player': max_rebounds_player,
        'max_rebounds_season': max_rebounds_season,
        'max_defensive_rebounds': max_defensive_rebounds,
        'max_defensive_rebounds_player': max_defensive_rebounds_player,
        'max_defensive_rebounds_season': max_defensive_rebounds_season,
        'max_offensive_rebounds': max_offensive_rebounds,
        'max_offensive_rebounds_player': max_offensive_rebounds_player,
        'max_offensive_rebounds_season': max_offensive_rebounds_season,
        'max_steals': max_steals,
        'max_steals_player': max_steals_player,
        'max_steals_season': max_steals_season,
        'max_blocks': max_blocks,
        'max_blocks_player': max_blocks_player,
        'max_blocks_season': max_blocks_season,
        'max_points': max_points,
        'max_points_player': max_points_player,
        'max_points_season': max_points_season,
    }
