from django.db import models

from calculator.models import TacticPlay


def get_moral_handicap(moral, game_location) -> int:
    from calculator.models import MoralPenalty
    return MoralPenalty.objects.get(moral=moral, game_location=game_location).penalty


def get_experience_handicap(exp: int) -> int:
    if exp is None:
        exp = 0
    if exp < 21:
        return 12
    elif exp < 31:
        return 9
    elif exp < 41:
        return 8
    elif exp < 51:
        return 7
    elif exp < 61:
        return 5
    elif exp < 71:
        return 4
    elif exp < 81:
        return 3
    elif exp < 91:
        return 1
    else:
        return 0


def get_tactic_points(players_set: set, point_guard_pass, point_guard_steal, specific_match: bool) -> int:
    result = 0
    if specific_match:
        result = TacticPlay.objects.filter(players__id__in=players_set).values('id', 'value').annotate(
            cnt=models.Count('id')).filter(cnt=3).order_by('-value')[:8].aggregate(models.Sum('value')).get(
            'value__sum')

    result = 0 if result is None else result / 6

    result += point_guard_pass / 6 + point_guard_steal / 6
    return result
