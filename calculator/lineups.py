import datetime
from itertools import permutations, islice

from django.db.models import Q

from calculator.constants import MY_TEAM, mafia, MY_BUDGET, MAX_MARKET_AGE, MARKET_PRECISION_LEVEL
from calculator.player import get_players_ratings, get_players_ratings_center, get_players_ratings_forward, \
    get_players_ratings_guards
from calculator.player_modificators import get_tactic_points
from calculator.player_shoot import calculate_shoots_distribution_simple
from .models import Player


def calculate_team_current(team: str, specific_match: bool, **kwargs) -> dict:
    if team == 'Mafia':
        players = Player.objects.filter(Q(team=MY_TEAM) | Q(team=MY_TEAM + ' B') | Q(team__in=mafia))
    elif team == 'Lega':
        players = Player.objects.all()
    else:
        if specific_match:
            players = Player.objects.filter(Q(Q(team=team) & Q(enabled=True)))
        else:
            players = Player.objects.filter(Q(Q(team=team) | Q(team=team + ' B') | Q(signed=team)))

    starting = True
    if 'excluded_players' in kwargs:
        starting = False
        for e in kwargs.get('excluded_players'):
            players = players.exclude(pk=int(e))

    centers = players.filter(position='P')
    forwards = players.filter(Q(position='AP') | Q(position='A'))
    guards = players.filter(Q(position='B') | Q(position='E'))
    tmp_player = None
    tmp_player_1 = None
    tmp_player_2 = None
    selected_players = set()

    ratings_centers = get_players_ratings_center(centers, True, starting, specific_match)

    p_off = [(ratings_centers[p].get('id'), ratings_centers[p]) for p in
             sorted(ratings_centers, key=lambda x: ratings_centers[x].get('P_O').get('total'), reverse=True)]
    p_def = [(ratings_centers[p].get('id'), ratings_centers[p]) for p in
             sorted(ratings_centers, key=lambda x: ratings_centers[x].get('P_D').get('total'), reverse=True)]

    keep_players = set()
    for aux in islice(p_off, 0, 1):
        keep_players.add(aux[0])
    for aux in islice(p_def, 0, 1):
        keep_players.add(aux[0])
    ratings_centers = {k: ratings_centers[k] for k in keep_players}

    center = {}
    score = 0
    total = 0
    for p in ratings_centers.items():
        aux = p[1].get('P').get('total')
        if aux > score:
            score = aux
            center = {
                'name': p[1].get('name'),
                'data': p[1].get('P'),
            }
            tmp_player = p[0]
        aux = p[1].get('P_D').get('total')
        if aux > score:
            score = aux
            center = {
                'name': p[1].get('name'),
                'data': p[1].get('P_D'),
            }
            tmp_player = p[0]
    selected_players.add(tmp_player)

    total += score
    power_forward = {}
    small_forward = {}

    ratings_forwards = get_players_ratings_forward(forwards, True, starting, specific_match)
    ap_o = [(ratings_forwards[p].get('id'), ratings_forwards[p]) for p in
            sorted(ratings_forwards, key=lambda x: ratings_forwards[x].get('AP_O').get('total'), reverse=True)]

    a_o = [(ratings_forwards[p].get('id'), ratings_forwards[p]) for p in
           sorted(ratings_forwards, key=lambda x: ratings_forwards[x].get('A_O').get('total'), reverse=True)]

    ap_d = [(ratings_forwards[p].get('id'), ratings_forwards[p]) for p in
            sorted(ratings_forwards, key=lambda x: ratings_forwards[x].get('AP_D').get('total'), reverse=True)]

    a_d = [(ratings_forwards[p].get('id'), ratings_forwards[p]) for p in
           sorted(ratings_forwards, key=lambda x: ratings_forwards[x].get('A_D').get('total'), reverse=True)]

    keep_players = set()
    for aux in islice(ap_o, 0, 10):
        keep_players.add(aux[0])
    for aux in islice(a_o, 0, 10):
        keep_players.add(aux[0])
    for aux in islice(ap_d, 0, 10):
        keep_players.add(aux[0])
    for aux in islice(a_d, 0, 10):
        keep_players.add(aux[0])
    ratings_forwards = {k: ratings_forwards[k] for k in keep_players}

    score = 0
    for f in permutations(ratings_forwards.items(), 2):
        p = f[0]
        j = f[1]
        aux_1_def = j[1].get('A_D').get('total')
        aux_2_def = p[1].get('AP_D').get('total')

        aux_1_off = j[1].get('A_O').get('total')
        aux_2_off = p[1].get('AP_O').get('total')

        if aux_1_def + aux_2_off > score:
            score = aux_1_def + aux_2_off
            power_forward = {
                'name': p[1].get('name'),
                'data': p[1].get('AP_O'),
            }
            small_forward = {
                'name': j[1].get('name'),
                'data': j[1].get('A_D'),
            }
            tmp_player_1 = p[0]
            tmp_player_2 = j[0]

        if aux_2_def + aux_1_off > score:
            score = aux_2_def + aux_1_off
            power_forward = {
                'name': p[1].get('name'),
                'data': p[1].get('AP_D'),
            }
            small_forward = {
                'name': j[1].get('name'),
                'data': j[1].get('A_O'),
            }
            tmp_player_1 = p[0]
            tmp_player_2 = j[0]

    selected_players.add(tmp_player_1)
    selected_players.add(tmp_player_2)
    total += score

    shooting_guard = {}
    point_guard = {}

    ratings_guards = get_players_ratings_guards(guards, True, starting, specific_match)
    e_off = [(ratings_guards[p].get('id'), ratings_guards[p]) for p in
             sorted(ratings_guards, key=lambda x: ratings_guards[x].get('E_O').get('total'), reverse=True)]

    b_off = [(ratings_guards[p].get('id'), ratings_guards[p]) for p in
             sorted(ratings_guards, key=lambda x: ratings_guards[x].get('B_O').get('total'), reverse=True)]

    e_def = [(ratings_guards[p].get('id'), ratings_guards[p]) for p in
             sorted(ratings_guards, key=lambda x: ratings_guards[x].get('E_D').get('total'), reverse=True)]

    b_def = [(ratings_guards[p].get('id'), ratings_guards[p]) for p in
             sorted(ratings_guards, key=lambda x: ratings_guards[x].get('B_D').get('total'), reverse=True)]

    keep_players = set()
    for aux in islice(e_def, 0, 10):
        keep_players.add(aux[0])
    for aux in islice(b_def, 0, 10):
        keep_players.add(aux[0])
    for aux in islice(e_off, 0, 10):
        keep_players.add(aux[0])
    for aux in islice(b_off, 0, 10):
        keep_players.add(aux[0])
    ratings_guards = {k: ratings_guards[k] for k in keep_players}

    score = 0
    for g in permutations(ratings_guards.items(), 2):
        p = g[0]
        j = g[1]
        aux_1_def = j[1].get('B_D').get('total')
        aux_2_def = p[1].get('E_D').get('total')

        aux_1_off = j[1].get('B_O').get('total')
        aux_2_off = p[1].get('E_O').get('total')

        tmp_total = aux_1_def + aux_2_off
        player_set = {p[1].get('id'), j[1].get('id')}
        tactic = get_tactic_points(player_set, j[1].get('passing'), j[1].get('steal'), False)
        tmp_total += tactic
        if tmp_total > score:
            score = tmp_total
            shooting_guard = {
                'name': p[1].get('name'),
                'data': p[1].get('E_O'),
            }
            point_guard = {
                'name': j[1].get('name'),
                'data': j[1].get('B_D'),
            }
            tmp_player_1 = p[0]
            tmp_player_2 = j[0]

        tmp_total = aux_2_def + aux_1_off
        tmp_total += tactic
        if tmp_total > score:
            score = tmp_total
            shooting_guard = {
                'name': p[1].get('name'),
                'data': p[1].get('E_D'),
            }
            point_guard = {
                'name': j[1].get('name'),
                'data': j[1].get('B_O'),
            }
            tmp_player_1 = p[0]
            tmp_player_2 = j[0]

    selected_players.add(tmp_player_1)
    selected_players.add(tmp_player_2)
    total += score

    lineup = {'total': total, 'center': center, 'power_forward': power_forward, 'small_forward': small_forward,
              'shooting_guard': shooting_guard, 'point_guard': point_guard}
    if 'excluded_players' not in kwargs:
        lineup.update(
            {'reserves': calculate_team_current(team, specific_match, excluded_players=selected_players)})
    return lineup


def calculate_team_potential(team: str, market: bool = False, budget: float = MY_BUDGET, starting=True,
                             **kwargs) -> dict:
    if team == '*':
        team_players = Player.objects.filter(
            (Q(price__lte=budget) & Q(age__lte=MAX_MARKET_AGE) & Q(Q(signed='-') | Q(signed=None)
                                                                   | Q(signed=''))) | Q(
                team=MY_TEAM) | Q(team=MY_TEAM + ' B') | Q(signed=MY_TEAM))
        team_players = team_players.exclude(Q(Q(team__in=mafia) & ~Q(team=MY_TEAM) & ~Q(team=MY_TEAM + ' B')))
    elif team == 'Mafia':
        team_players = Player.objects.filter(Q(team=MY_TEAM) | Q(team=MY_TEAM + ' B') | Q(team__in=mafia))
    elif team == 'Lega':
        team_players = Player.objects.all()
    else:
        team_players = Player.objects.filter(Q(team=team) | Q(team=team + ' B') | Q(signed=team))

    if 'excluded_players' in kwargs:
        for e in kwargs.get('excluded_players'):
            team_players = team_players.exclude(pk=e)

    selected_players = set()

    center = {}
    power_forward = {}
    small_forward = {}
    shooting_guard = {}
    point_guard = {}

    players = get_players_ratings(team_players, False, starting)

    if len(players) > 10:
        p_def = [(players[p].get('id'), players[p]) for p in
                 sorted(players, key=lambda x: players[x].get('P_D').get('total'), reverse=True)]

        ap_def = [(players[p].get('id'), players[p]) for p in
                  sorted(players, key=lambda x: players[x].get('AP_D').get('total'), reverse=True)]
        a_def = [(players[p].get('id'), players[p]) for p in
                 sorted(players, key=lambda x: players[x].get('A_D').get('total'), reverse=True)]
        e_def = [(players[p].get('id'), players[p]) for p in
                 sorted(players, key=lambda x: players[x].get('E_D').get('total'), reverse=True)]
        b_def = [(players[p].get('id'), players[p]) for p in
                 sorted(players, key=lambda x: players[x].get('B_D').get('total'), reverse=True)]

        p_off = [(players[p].get('id'), players[p]) for p in
                 sorted(players, key=lambda x: players[x].get('P').get('total'), reverse=True)]

        ap_off = [(players[p].get('id'), players[p]) for p in
                  sorted(players, key=lambda x: players[x].get('AP_O').get('total'), reverse=True)]
        a_off = [(players[p].get('id'), players[p]) for p in
                 sorted(players, key=lambda x: players[x].get('A_O').get('total'), reverse=True)]
        e_off = [(players[p].get('id'), players[p]) for p in
                 sorted(players, key=lambda x: players[x].get('E_O').get('total'), reverse=True)]
        b_off = [(players[p].get('id'), players[p]) for p in
                 sorted(players, key=lambda x: players[x].get('B_O').get('total'), reverse=True)]

        keep_players = set()
        if market:

            my_team = {k: v for k, v in players.items() if v.get('team') == MY_TEAM}

            for aux in my_team.keys():
                keep_players.add(aux)

        for aux in islice(p_off, 0, 5):
            keep_players.add(aux[0])
        for aux in islice(ap_off, 0, 5):
            keep_players.add(aux[0])
        for aux in islice(a_off, 0, 5):
            keep_players.add(aux[0])
        for aux in islice(e_off, 0, 5):
            keep_players.add(aux[0])
        for aux in islice(b_off, 0, 5):
            keep_players.add(aux[0])
        for aux in islice(p_def, 0, 5):
            keep_players.add(aux[0])
        for aux in islice(ap_def, 0, 5):
            keep_players.add(aux[0])
        for aux in islice(a_def, 0, 5):
            keep_players.add(aux[0])
        for aux in islice(e_def, 0, 5):
            keep_players.add(aux[0])
        for aux in islice(b_def, 0, 5):
            keep_players.add(aux[0])

        players = {k: players[k] for k in keep_players}

    total = 0
    price = 0
    for aux in permutations(players.items(), 5):
        tmp_b_def = aux[0][1].get('B_D')
        tmp_e_def = aux[1][1].get('E_D')
        tmp_a_def = aux[2][1].get('A_D')
        tmp_ap_def = aux[3][1].get('AP_D')
        tmp_p_def = aux[4][1].get('P_D')

        tmp_b_off = aux[0][1].get('B_O')
        tmp_e_off = aux[1][1].get('E_O')
        tmp_a_off = aux[2][1].get('A_O')
        tmp_ap_off = aux[3][1].get('AP_O')
        tmp_p_off = aux[4][1].get('P')

        if tmp_p_def.get('total') > tmp_p_off.get('total'):
            tmp_p = tmp_p_def
        else:
            tmp_p = tmp_p_off

        aap = tmp_a_off.get('total') + tmp_ap_def.get('total')
        apa = tmp_a_def.get('total') + tmp_ap_off.get('total')

        if aap > apa:
            tmp_ap = tmp_ap_def
            tmp_a = tmp_a_off
            tmp_forwards = aap
        else:
            tmp_a = tmp_a_def
            tmp_ap = tmp_ap_off
            tmp_forwards = apa

        be = tmp_b_off.get('total') + tmp_e_def.get('total')
        eb = tmp_b_def.get('total') + tmp_e_off.get('total')

        if eb > be:
            tmp_b = tmp_b_def
            tmp_e = tmp_e_off
            tmp_guards = eb
        else:
            tmp_e = tmp_e_def
            tmp_b = tmp_b_off
            tmp_guards = be
        tmp_total = tmp_guards + tmp_forwards + tmp_p.get('total')

        players_set = {tmp_b.get('id'), tmp_e.get('id'), tmp_a.get('id'), tmp_ap.get('id'), tmp_p.get('id')}

        tmp_total += get_tactic_points(players_set, aux[0][1].get('passing'), aux[0][1].get('steal'), False)

        if market:
            tmp_price = aux[0][1].get('price') + aux[1][1].get('price') + aux[2][1].get('price') + aux[3][1].get(
                'price') + aux[4][1].get('price')
        else:
            tmp_price = -1

        if tmp_total > total and (tmp_price <= MY_BUDGET):
            total = tmp_total
            price = tmp_price
            center = {'name': aux[4][1].get('name'), 'data': tmp_p}
            power_forward = {'name': aux[3][1].get('name'), 'data': tmp_ap}
            small_forward = {'name': aux[2][1].get('name'), 'data': tmp_a}
            shooting_guard = {'name': aux[1][1].get('name'), 'data': tmp_e}
            point_guard = {'name': aux[0][1].get('name'), 'data': tmp_b}
            selected_players = set()
            selected_players.add(aux[0][0])
            selected_players.add(aux[1][0])
            selected_players.add(aux[2][0])
            selected_players.add(aux[3][0])
            selected_players.add(aux[4][0])

    lineup = {'total': total, 'center': center, 'power_forward': power_forward, 'small_forward': small_forward,
              'shooting_guard': shooting_guard, 'point_guard': point_guard}

    if 'excluded_players' not in kwargs:
        if market:
            lineup.update(
                {'reserves': calculate_team_potential(team, market, budget - price, False,
                                                      excluded_players=selected_players)})
        else:
            lineup.update({'reserves': calculate_team_potential(team, market, 0, False,
                                                                excluded_players=selected_players)})

    return lineup


def calculate_team_specific_match(team: str, starting: bool, **kwargs) -> dict:
    team_players = Player.objects.filter(Q(team=team) & Q(enabled=True))

    if 'excluded_players' in kwargs:
        for e in kwargs.get('excluded_players'):
            team_players = team_players.exclude(pk=e)

    selected_players = set()

    center = {}
    power_forward = {}
    small_forward = {}
    shooting_guard = {}
    point_guard = {}

    players = get_players_ratings(team_players, True, starting, True)

    total = 0

    for aux in permutations(players.items(), 5):
        tmp_b_def = aux[0][1].get('B_D')
        tmp_e_def = aux[1][1].get('E_D')
        tmp_a_def = aux[2][1].get('A_D')
        tmp_ap_def = aux[3][1].get('AP_D')
        tmp_p_def = aux[4][1].get('P_D')

        if tmp_b_def.get('total') != 0 and tmp_e_def.get('total') != 0 and tmp_a_def.get(
                'total') != 0 and tmp_ap_def.get('total') != 0 and tmp_p_def.get('total') != 0:

            tmp_b_off = aux[0][1].get('B_O')
            tmp_e_off = aux[1][1].get('E_O')
            tmp_a_off = aux[2][1].get('A_O')
            tmp_ap_off = aux[3][1].get('AP_O')
            tmp_p_off = aux[4][1].get('P_O')

            if tmp_p_def.get('total') > tmp_p_off.get('total'):
                tmp_p = tmp_p_def
            else:
                tmp_p = tmp_p_off

            aap = tmp_a_off.get('total') + tmp_ap_def.get('total')
            apa = tmp_a_def.get('total') + tmp_ap_off.get('total')

            if aap > apa:
                tmp_ap = tmp_ap_def
                tmp_a = tmp_a_off
                tmp_forwards = aap
            else:
                tmp_a = tmp_a_def
                tmp_ap = tmp_ap_off
                tmp_forwards = apa

            be = tmp_b_off.get('total') + tmp_e_def.get('total')
            eb = tmp_b_def.get('total') + tmp_e_off.get('total')

            if eb > be:
                tmp_b = tmp_b_def
                tmp_e = tmp_e_off
                tmp_guards = eb
            else:
                tmp_e = tmp_e_def
                tmp_b = tmp_b_off
                tmp_guards = be
            tmp_total = tmp_guards + tmp_forwards + tmp_p.get('total')

            players_set = {tmp_b.get('id'), tmp_e.get('id'), tmp_a.get('id'), tmp_ap.get('id'),
                           tmp_p.get('id')}

            tmp_total += get_tactic_points(players_set, aux[0][1].get('passing'), aux[0][1].get('steal'), True)

            if tmp_total > total:
                center = {'name': aux[4][1].get('name'), 'data': tmp_p}
                power_forward = {'name': aux[3][1].get('name'), 'data': tmp_ap}
                small_forward = {'name': aux[2][1].get('name'), 'data': tmp_a}
                shooting_guard = {'name': aux[1][1].get('name'), 'data': tmp_e}
                point_guard = {'name': aux[0][1].get('name'), 'data': tmp_b}
                selected_players = set()
                selected_players.add(aux[0][0])
                selected_players.add(aux[1][0])
                selected_players.add(aux[2][0])
                selected_players.add(aux[3][0])
                selected_players.add(aux[4][0])

                total = tmp_total

    lineup = {'total': total, 'center': center, 'power_forward': power_forward, 'small_forward': small_forward,
              'shooting_guard': shooting_guard, 'point_guard': point_guard}

    if 'excluded_players' not in kwargs:
        lineup.update({'reserves': calculate_team_specific_match(team, False, excluded_players=selected_players)})

    return lineup


def calculate_team(team: str, starting: bool, current: bool, specific_match: bool, market: bool, budget: float = 0,
                   redistribute: bool = False, team_players: dict = None,
                   **kwargs) -> dict:
    keep_players = set()

    aux_b_my_team = 100
    aux_e_my_team = 100
    aux_a_my_team = 100
    aux_ap_my_team = 100
    aux_p_my_team = 100

    if team_players is None or market:
        if specific_match:

            team_players = Player.objects.filter(Q(team=team) & Q(enabled=True) | Q(loaned=team)).prefetch_related(
                'position')
            """
            team_players = Player.objects.filter(
                Q(team=team) | Q(team=team + ' B') |  Q(loaned=team)).prefetch_related(
                'position')
            """

        elif market:
            team_players = Player.objects.filter(
                Q(team=MY_TEAM) | Q(team=MY_TEAM + ' B') | Q(signed=MY_TEAM) | Q(loaned=team) | (
                        Q(price__lte=budget) & Q(age__lte=MAX_MARKET_AGE)
                        & Q(Q(signed='-') | Q(signed=None) | Q(signed='')))).prefetch_related(
                'position')
            team_players = team_players.exclude(
                Q(Q(team__in=mafia) & ~Q(team=MY_TEAM) & ~Q(team=MY_TEAM + ' B'))).prefetch_related('position')

            my_players = Player.objects.filter(Q(team=MY_TEAM) | Q(team=MY_TEAM + ' B') | Q(signed=MY_TEAM))

            if 'excluded_players' in kwargs:
                for e in kwargs.get('excluded_players'):
                    my_players = my_players.exclude(pk=e)

            players = get_players_ratings(my_players, current, starting, specific_match, redistribute)

            players_to_keep = get_players_to_keep(players)
            keep_players = players_to_keep.get('keep_players').copy()
            aux_b_my_team = players_to_keep.get('aux_b')
            aux_e_my_team = players_to_keep.get('aux_e')
            aux_a_my_team = players_to_keep.get('aux_a')
            aux_ap_my_team = players_to_keep.get('aux_ap')
            aux_p_my_team = players_to_keep.get('aux_p')

        elif team == 'Mafia':
            team_players = Player.objects.filter(
                Q(team=MY_TEAM) | Q(team=MY_TEAM + ' B') | Q(team__in=mafia)).prefetch_related('position')
        elif team == 'Lega':
            team_players = Player.objects.all().prefetch_related('position')
        else:
            team_players = Player.objects.filter(Q(team=team) | Q(team=team + ' B') | Q(signed=team)).prefetch_related(
                'position')
            # .order_by('position__number')

    if 'excluded_players' in kwargs:
        for e in kwargs.get('excluded_players'):
            team_players = team_players.exclude(pk=e)

    print('Players get - ' + ('Main' if starting else 'Reserves'))
    selected_players = set()

    center = {}
    power_forward = {}
    small_forward = {}
    shooting_guard = {}
    point_guard = {}

    players = get_players_ratings(team_players, current, starting, specific_match, redistribute)

    if market:
        my_team = {k: v for k, v in players.items() if v.get('team') in (MY_TEAM, MY_TEAM + '_B')}
        for aux in my_team.keys():
            keep_players.add(aux)

    players_to_keep = get_players_to_keep(players, market=market)
    keep_players = players_to_keep.get('keep_players').copy()
    aux_b = players_to_keep.get('aux_b')
    aux_e = players_to_keep.get('aux_e')
    aux_a = players_to_keep.get('aux_a')
    aux_ap = players_to_keep.get('aux_ap')
    aux_p = players_to_keep.get('aux_p')
    number_players_position = players_to_keep.get('players_position')

    if market:
        my_team = {k: v for k, v in players.items() if v.get('team') in (MY_TEAM, MY_TEAM + '_B')}
        for aux in my_team.keys():
            keep_players.add(aux)

    players = {k: players[k] for k in keep_players}

    total = 0
    price = 0

    i = 0

    print('Players filtered - ' + ('Main' if starting else 'Reserves') + ' - Number: ' + str(len(players)))
    print(('Main' if starting else 'Reserves') + ' - ' + str(i) + ' - ' + str(datetime.datetime.now()))
    for aux in permutations(players.items(), 5):
        tmp_b = aux[0][1].get('B')
        tmp_e = aux[1][1].get('E')
        tmp_a = aux[2][1].get('A')
        tmp_ap = aux[3][1].get('AP')
        tmp_p = aux[4][1].get('P')

        if market:
            tmp_price = aux[0][1].get('price') + aux[1][1].get('price') + aux[2][1].get('price') + aux[3][1].get(
                'price') + aux[4][1].get('price')
        else:
            tmp_price = 0

        eval_b = (market and tmp_b.get('team') in (MY_TEAM, MY_TEAM + '_B') and aux_b_my_team > tmp_b.get(
            'total')) or tmp_b.get('total') >= aux_b

        eval_e = (market and tmp_e.get('team') in (MY_TEAM, MY_TEAM + '_B') and tmp_e.get(
            'total') >= aux_e_my_team) or tmp_e.get('total') >= aux_e
        eval_a = (market and tmp_a.get('team') in (MY_TEAM, MY_TEAM + '_B') and tmp_a.get(
            'total') >= aux_a_my_team) or tmp_a.get('total') >= aux_a
        eval_ap = (market and tmp_ap.get('team') in (MY_TEAM, MY_TEAM + '_B') and tmp_ap.get(
            'total') >= aux_ap_my_team) or tmp_ap.get('total') >= aux_ap
        eval_p = (market and tmp_p.get('team') in (MY_TEAM, MY_TEAM + '_B') and
                  tmp_p.get('total') >= aux_p_my_team) or tmp_p.get('total') >= aux_p

        if tmp_price <= budget and eval_b and eval_e and eval_a and eval_ap and eval_p:
            ratings = {tmp_b.get('total'), tmp_e.get('total'), tmp_a.get('total'), tmp_ap.get('total'),
                       tmp_p.get('total')}
            min_ratings = min(ratings)
            if min_ratings != 0:

                tmp_center = {'name': aux[4][1].get('name'), 'data': tmp_p}
                tmp_power_forward = {'name': aux[3][1].get('name'), 'data': tmp_ap}
                tmp_small_forward = {'name': aux[2][1].get('name'), 'data': tmp_a}
                tmp_shooting_guard = {'name': aux[1][1].get('name'), 'data': tmp_e}
                tmp_point_guard = {'name': aux[0][1].get('name'), 'data': tmp_b}

                tmp_lineup = {'center': tmp_center, 'power_forward': tmp_power_forward,
                              'small_forward': tmp_small_forward,
                              'shooting_guard': tmp_shooting_guard, 'point_guard': tmp_point_guard}

                distribution = calculate_shoots_distribution_simple(tmp_lineup)

                total_center = get_player_rating(distribution.get('center'), tmp_p.get('real_rating'),
                                                 tmp_p.get('defensive'),
                                                 tmp_p.get('support'))
                total_power_forward = get_player_rating(distribution.get('power_forward'), tmp_ap.get('real_rating'),
                                                        tmp_ap.get('defensive'),
                                                        tmp_ap.get('support'))
                total_small_forward = get_player_rating(distribution.get('small_forward'), tmp_a.get('real_rating'),
                                                        tmp_a.get('defensive'),
                                                        tmp_a.get('support'))
                total_shooting_guard = get_player_rating(distribution.get('shooting_guard'), tmp_e.get('real_rating'),
                                                         tmp_e.get('defensive'),
                                                         tmp_e.get('support'))
                total_point_guard = get_player_rating(distribution.get('point_guard'), tmp_b.get('real_rating'),
                                                      tmp_b.get('defensive'),
                                                      tmp_b.get('support'))

                tmp_total = total_center + total_power_forward + total_small_forward + total_shooting_guard + total_point_guard

                players_set = {tmp_b.get('id'), tmp_e.get('id'), tmp_a.get('id'), tmp_ap.get('id'),
                               tmp_p.get('id')}

                tmp_total += get_tactic_points(players_set, aux[0][1].get('passing'), aux[0][1].get('steal'),
                                               specific_match)

                if tmp_total > total:
                    center = {'name': aux[4][1].get('name'), 'data': tmp_p}
                    power_forward = {'name': aux[3][1].get('name'), 'data': tmp_ap}
                    small_forward = {'name': aux[2][1].get('name'), 'data': tmp_a}
                    shooting_guard = {'name': aux[1][1].get('name'), 'data': tmp_e}
                    point_guard = {'name': aux[0][1].get('name'), 'data': tmp_b}
                    selected_players = set()
                    selected_players.add(aux[0][0])
                    selected_players.add(aux[1][0])
                    selected_players.add(aux[2][0])
                    selected_players.add(aux[3][0])
                    selected_players.add(aux[4][0])
                    price = tmp_price
                    total = tmp_total
                    i += 1

                    print(('Main' if starting else 'Reserves') + '--' + str(
                        number_players_position) + '------- ' + str(
                        i) + ' - Current team ---------' + str(datetime.datetime.now()))
                    dict_player = point_guard.get('data')
                    print('B: ' + point_guard.get('name') + ' team: ' + dict_player.get(
                        'team'))
                    dict_player = shooting_guard.get('data')
                    print('E: ' + shooting_guard.get('name') + ' team: ' + dict_player.get(
                        'team'))
                    dict_player = small_forward.get('data')
                    print('A: ' + small_forward.get('name') + ' team: ' + dict_player.get(
                        'team'))
                    dict_player = power_forward.get('data')
                    print('AP: ' + power_forward.get('name') + ' team: ' + dict_player.get(
                        'team'))
                    dict_player = center.get('data')
                    print('P ' + center.get('name') + ' team: ' + dict_player.get(
                        'team'))
                    print('Budget: ' + str(price))

    print('Lineup calculated - ' + ('Main' if starting else 'Reserves') + ' P:' + center.get(
        'name') + ' AP:' + power_forward.get(
        'name') + ' A:' + small_forward.get('name') + ' E:' + shooting_guard.get('name') + ' B:' + point_guard.get(
        'name'))

    lineup_total = center.get('data').get('total') + power_forward.get('data').get('total') + small_forward.get(
        'data').get('total') + shooting_guard.get('data').get('total') + point_guard.get('data').get('total')
    lineup = {'total': lineup_total, 'center': center, 'power_forward': power_forward, 'small_forward': small_forward,
              'shooting_guard': shooting_guard, 'point_guard': point_guard}

    if 'excluded_players' not in kwargs:
        lineup.update(
            {'reserves': calculate_team(team, False, current, specific_match, market, budget=(budget - price),
                                        team_players=team_players, redistribute=redistribute,
                                        excluded_players=selected_players)})

    return lineup


def get_player_rating(shoot_incidence: int, offensive: float, defensive: float, support: float) -> float:
    return (offensive * (0.5 * (shoot_incidence - 1)) + defensive + support) / 3


def get_players_to_keep(players: dict, market=False, keep_players=None):
    aux_b = 100
    aux_e = 100
    aux_a = 100
    aux_ap = 100
    aux_p = 100

    if keep_players is None:
        bkp_keep_players = set()
    else:
        bkp_keep_players = keep_players.copy()

    p_def = [(players[p].get('id'), players[p]) for p in
             sorted(players, key=lambda x: players[x].get('P_D').get('total'), reverse=True)]

    ap_def = [(players[p].get('id'), players[p]) for p in
              sorted(players, key=lambda x: players[x].get('AP_D').get('total'), reverse=True)]
    a_def = [(players[p].get('id'), players[p]) for p in
             sorted(players, key=lambda x: players[x].get('A_D').get('total'), reverse=True)]
    e_def = [(players[p].get('id'), players[p]) for p in
             sorted(players, key=lambda x: players[x].get('E_D').get('total'), reverse=True)]
    b_def = [(players[p].get('id'), players[p]) for p in
             sorted(players, key=lambda x: players[x].get('B_D').get('total'), reverse=True)]

    p_off = [(players[p].get('id'), players[p]) for p in
             sorted(players, key=lambda x: players[x].get('P').get('total'), reverse=True)]

    ap_off = [(players[p].get('id'), players[p]) for p in
              sorted(players, key=lambda x: players[x].get('AP_O').get('total'), reverse=True)]
    a_off = [(players[p].get('id'), players[p]) for p in
             sorted(players, key=lambda x: players[x].get('A_O').get('total'), reverse=True)]
    e_off = [(players[p].get('id'), players[p]) for p in
             sorted(players, key=lambda x: players[x].get('E_O').get('total'), reverse=True)]
    b_off = [(players[p].get('id'), players[p]) for p in
             sorted(players, key=lambda x: players[x].get('B_O').get('total'), reverse=True)]

    number_players_position = 10 if market else 5

    keep_players = set()
    while (market and len(keep_players) > MARKET_PRECISION_LEVEL) or len(keep_players) == 0:
        keep_players = bkp_keep_players.copy()
        for aux in islice(p_off, 0, number_players_position):
            keep_players.add(aux[0])
            aux_b = min(aux_b, aux[1].get('B').get('total'))
        for aux in islice(ap_off, 0, number_players_position):
            keep_players.add(aux[0])
            aux_b = min(aux_b, aux[1].get('B').get('total'))
        for aux in islice(a_off, 0, number_players_position):
            keep_players.add(aux[0])
            aux_e = min(aux_e, aux[1].get('E').get('total'))
        for aux in islice(e_off, 0, number_players_position):
            keep_players.add(aux[0])
            aux_e = min(aux_e, aux[1].get('E').get('total'))
        for aux in islice(b_off, 0, number_players_position):
            keep_players.add(aux[0])
            aux_a = min(aux_a, aux[1].get('A').get('total'))
        for aux in islice(p_def, 0, number_players_position):
            keep_players.add(aux[0])
            aux_a = min(aux_a, aux[1].get('A').get('total'))
        for aux in islice(ap_def, 0, number_players_position):
            keep_players.add(aux[0])
            aux_ap = min(aux_ap, aux[1].get('AP').get('total'))
        for aux in islice(a_def, 0, number_players_position):
            keep_players.add(aux[0])
            aux_ap = min(aux_ap, aux[1].get('AP').get('total'))
        for aux in islice(e_def, 0, number_players_position):
            keep_players.add(aux[0])
            aux_p = min(aux_p, aux[1].get('P').get('total'))
        for aux in islice(b_def, 0, number_players_position):
            keep_players.add(aux[0])
            aux_p = min(aux_p, aux[1].get('P').get('total'))
        number_players_position -= 1
    result = {
        'keep_players': keep_players,
        'aux_b': aux_b,
        'aux_e': aux_e,
        'aux_a': aux_a,
        'aux_ap': aux_ap,
        'aux_p': aux_p,
        'players_position': number_players_position
    }
    return result
