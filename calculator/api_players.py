from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_200_OK

from calculator.constants import TEAM_SHOTS
from calculator.models import Player, Position, OffensiveConfiguration, Season, Competition, PlayerSeason
from calculator.player import calculate_skills
from calculator.player_shoot import get_three_point_shoot_percentage, get_interior_shoot_percentage, \
    get_two_point_shoot_percentage


@api_view(['POST'])
def update_player(request):
    id_player = request.data['id']

    try:
        player = Player.objects.get(pk=id_player)
    except Player.DoesNotExist:
        return Response(status=HTTP_400_BAD_REQUEST)

    try:
        player.name = request.data['name']
        player.position = Position.objects.get(code=request.data['pos'])
        player.experience = request.data['exp']
        player.onePointShoot = request.data['t1']
        player.twoPointShoot = request.data['t2']
        player.threePointShoot = request.data['t3']
        player.dunk = request.data['dunk']
        player.rebound = request.data['rebound']
        player.block = request.data['block']
        player.passing = request.data['passing']
        player.steal = request.data['steal']
        player.price = request.data['price']
        player.age = request.data['age']
        player.motivated = request.data['motivated'] == "true"
        player.enabled = request.data['enabled'] == "true"
        player.team = request.data['team']
        player.signed = request.data['signed']
        player.loaned = request.data['loaned']
        player.save()
        return Response(True, status=HTTP_200_OK)
    except Player.ValidationError:
        return Response(status=HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def add_player(request):
    try:
        player = Player(
            name=request.data['name'],
            position=Position.objects.get(code=request.data['pos']),
            experience=request.data['exp'],
            onePointShoot=request.data['t1'],
            twoPointShoot=request.data['t2'],
            threePointShoot=request.data['t3'],
            dunk=request.data['dunk'],
            rebound=request.data['rebound'],
            block=request.data['block'],
            passing=request.data['passing'],
            steal=request.data['steal'],
            price=request.data['price'],
            age=request.data['age'],
            motivated=request.data['motivated'] == "true",
            team=request.data['team'],
            signed=request.data['signed'],
            loaned=request.data['loaned'],
            enabled=request.data['enabled'] == "true",
        )
        player.save()
        return Response(True, status=HTTP_200_OK)
    except Player.ValidationError:
        return Response(status=HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def get_player_match_shoot(request):
    shooter = Player.objects.get(pk=request.query_params['id'])
    block = int(request.query_params['block'])
    time_percentage = float(request.query_params['time'])
    player_shoots = float(request.query_params['shoot'])
    position = request.query_params['position']

    skills = calculate_skills(shooter, True)

    interior = get_interior_shoot_percentage(skills.get('dunk'), block)
    two_point = get_two_point_shoot_percentage(skills.get('twoPointShoot'), block)
    three_point = get_three_point_shoot_percentage(skills.get('threePointShoot'), block)

    number_shoots = TEAM_SHOTS * time_percentage * player_shoots

    total = 0
    result = {}
    for c in OffensiveConfiguration.objects.filter(position=position):
        exterior = (c.exteriorRate / 100 * number_shoots * three_point) * 3
        mid = (c.midRate / 100 * number_shoots * two_point) * 2
        interior_points = (c.interiorRate / 100 * number_shoots * interior) * 2

        aux = exterior + mid + interior_points
        if aux >= total:
            total = aux
            result = {'style': c.name, 'total': round(total, 2)}

    return Response(result, status=HTTP_200_OK)


@api_view(['POST'])
def add_player_season(id_player, id_season, id_competition, games, offensive_rebound, defensive_rebound, assists,
                      steals, blocks, points):
    player = Player.objects.get(pk=id_player)
    season = Season.objects.get(code=id_season)
    competition = Competition.objects.get(code=id_competition)
    player_season = PlayerSeason(
        player=player,
        season=season,
        competition=competition,
        games=games,
        offensive_rebound=offensive_rebound,
        defensive_rebound=defensive_rebound,
        assists=assists,
        steals=steals,
        blocks=blocks,
        points=points
    )

    player_season.save()