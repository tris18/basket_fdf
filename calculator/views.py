from django.db.models import Sum
from django.shortcuts import render

from calculator.constants import MY_TEAM_URL
from calculator.models import PlayerSeason
from calculator.player import get_all_history_greatests, get_all_history_greatests_seasons
from calculator.player_shoot import calculate_shoots_distribution
from calculator.utils import from_underscore_to_space
from .lineups import *


def potential_team(request, team=MY_TEAM):
    # res = calculate_team_potential(from_underscore_to_space(team), False, 0, True)
    if team == 'Mafia':
        res = calculate_team_potential(team, False)
    else:
        res = calculate_team(from_underscore_to_space(team), True, False, False, False)
    shoot_distribution = calculate_shoots_distribution(res)
    res.update({'team_url': team, 'team': from_underscore_to_space(team), 'my_team': MY_TEAM,
                'shoot_distribution': shoot_distribution})
    return render(request, 'Calculator/team.html', res)


def current_team(request, team=MY_TEAM):
    # res = calculate_team_current(from_underscore_to_space(team), False)
    res = calculate_team(from_underscore_to_space(team), True, True, False, False)
    shoot_distribution = calculate_shoots_distribution(res)
    res.update({'team_url': team, 'team': from_underscore_to_space(team), 'my_team': MY_TEAM,
                'shoot_distribution': shoot_distribution})
    return render(request, 'Calculator/team.html', res)


def redistributed_team(request, team=MY_TEAM):
    # res = calculate_team_current(from_underscore_to_space(team), False)
    res = calculate_team(from_underscore_to_space(team), True, True, False, False, redistribute=True)
    shoot_distribution = calculate_shoots_distribution(res)
    res.update({'team_url': team, 'team': from_underscore_to_space(team), 'my_team': MY_TEAM,
                'shoot_distribution': shoot_distribution})
    return render(request, 'Calculator/team.html', res)


def current_team_match(request, team=MY_TEAM):
    # res = calculate_team_specific_match(from_underscore_to_space(team), True)
    res = calculate_team(from_underscore_to_space(team), True, True, True, False)
    shoot_distribution = calculate_shoots_distribution(res)
    res.update({'team_url': team, 'team': from_underscore_to_space(team), 'my_team': MY_TEAM,
                'shoot_distribution': shoot_distribution})
    return render(request, 'Calculator/team.html', res)


def market_team(request):
    # res = calculate_team_potential('*', True, MY_BUDGET, True)
    res = calculate_team('*', True, False, False, True, MY_BUDGET)
    shoot_distribution = calculate_shoots_distribution(res)
    res.update({'team_url': MY_TEAM_URL, 'my_team': MY_TEAM, 'shoot_distribution': shoot_distribution})
    return render(request, 'Calculator/team.html', res)


def roster_current(request, team_url=MY_TEAM):
    team = from_underscore_to_space(team_url)
    return render(request, 'Calculator/ratings.html',
                  {'data': get_players_ratings(Player.objects.filter(Q(team=team) | Q(team=team + ' B')), True),
                   'team_url': team_url})


def roster_potential(request, team_url=MY_TEAM):
    team = from_underscore_to_space(team_url)
    return render(request, 'Calculator/ratings.html',
                  {'data': get_players_ratings(
                      Player.objects.filter(Q(signed=team) | Q(team=team + ' B') | Q(team=team)), False),
                      'team_url': team_url})


def roster_editor(request, team_url=MY_TEAM):
    team = from_underscore_to_space(team_url)
    if team == 'Free':
        players = Player.objects.filter(Q(Q(team='-'))).order_by('position__number')
    elif team == 'all':
        players = Player.objects.all().order_by('position__number')
    else:
        players = Player.objects.filter(Q(Q(team=team) | Q(signed=team) | Q(loaned=team))).order_by('position__number')
    return render(request, 'Calculator/team_editor.html',
                  {'players': players,
                   'team': team,
                   'team_url': team_url})


def market(request):
    return render(request, 'Calculator/ratings.html',
                  {'data': get_players_ratings(Player.objects.filter(
                      Q(price__lte=MY_BUDGET) & Q(age__lte=MAX_MARKET_AGE) & Q(
                          Q(signed='-') | Q(signed=None) | Q(signed=''))
                      | Q(team=MY_TEAM) | Q(signed=MY_TEAM)),
                      False), 'team_url': MY_TEAM_URL})


def statistics_summary(request):
    # Player total
    total_all_players = PlayerSeason.objects.values('player_id', 'player__name').annotate(Sum('games'), Sum('assists'),
                                                                                          Sum('defensive_rebounds'),
                                                                                          Sum('offensive_rebounds'),
                                                                                          Sum('steals'),
                                                                                          Sum('blocks'),
                                                                                          Sum('points'),
                                                                                          )
    all_history_greatests = get_all_history_greatests(total_all_players)

    greatest_seasons_players = PlayerSeason.objects.values('player_id', 'player__name', 'season_id').annotate(
        Sum('games'), Sum('assists'),
        Sum('defensive_rebounds'),
        Sum('offensive_rebounds'),
        Sum('steals'),
        Sum('blocks'),
        Sum('points'),
    )

    greatest_seasons = get_all_history_greatests_seasons(greatest_seasons_players)
    return render(request, 'Calculator/statistics.html',
                  {'total_all_players': total_all_players, 'all_history_greatests': all_history_greatests,
                   'greatest_seasons_players': greatest_seasons,
                   'team_url': MY_TEAM_URL})
