from django.contrib import admin

from .models import *


class PlayerAdmin(admin.ModelAdmin):
    list_display = ['name', 'age', 'team', 'signed', 'position', 'onePointShoot', 'twoPointShoot', 'threePointShoot',
                    'dunk', 'rebound', 'block', 'passing', 'steal', 'price', 'experience', 'motivated', 'enabled',
                    'international', 'moral']


admin.site.register(Player, PlayerAdmin)


class PlayerSeasonAdmin(admin.ModelAdmin):
    list_display = ['player', 'competition', 'season', 'games', 'offensive_rebounds', 'defensive_rebounds', 'assists',
                    'steals', 'blocks', 'points']


admin.site.register(PlayerSeason, PlayerSeasonAdmin)


class CompetitionAdmin(admin.ModelAdmin):
    list_display = ['code', 'name']


admin.site.register(Competition, CompetitionAdmin)


class SeasonAdmin(admin.ModelAdmin):
    list_display = ['code', 'year_beginning', 'year_ending']


admin.site.register(Season, SeasonAdmin)
