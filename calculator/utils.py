def from_underscore_to_space(self):
    return self.replace('_', ' ')


def range_creator(maximum, minimum, points):
    step = (maximum - minimum) / (points - 1)
    result = []
    i = 0
    while i <= points:
        result.append(minimum + step * i)
        i += 1
    return result


def get_player_number(name: str) -> int:
    try:
        arr = name.split(' ')

        val = 0
        for c in arr[1]:
            val += ord(c)

        val = (val % 25) + 1

        res = [val, val, val, val + 25, val + 50, max(val + 75, 99)]

        group = 0
        for c in arr[0]:
            group += ord(c)

        return res[group % 5]
    except IndexError:
        return -1


def get_supporters_attendance(local_supporters: int, away_supporters: int, stadium_capacity: int, price: str) -> int:
    social_mass = ((local_supporters + away_supporters) / 5) * 2

    if price == 'H':
        social_mass = social_mass / 2
    elif price == 'M':
        social_mass = social_mass * 3 / 4

    return int(min(social_mass, stadium_capacity))


def get_match_income(local_supporters: int, away_supporters: int, stadium_capacity: int, price_type: str) -> float:
    attendance = get_supporters_attendance(local_supporters, away_supporters, stadium_capacity, price_type)
    if price_type == 'H':
        price = attendance * 15 * 3
    elif price_type == 'M':
        price = attendance * 10 * 3
    else:
        price = attendance * 5 * 3
    return price


def get_recommended_tickets_price(local_supporters: int, away_supporters: int, stadium_capacity: int) -> dict:
    prices = ['H', 'M', 'L']
    income = 0

    result = {'Price': 'L', 'Income': 0}

    for price in prices:
        aux = get_match_income(local_supporters, away_supporters, stadium_capacity, price)
        if aux >= income:
            result = {'Price': price, 'Income': aux}
            income = aux

    return result


