from django.db import models

from calculator.utils import get_player_number


class Position(models.Model):
    code = models.CharField(max_length=25, primary_key=True, unique=True)
    name = models.CharField(max_length=30)
    passingRate = models.IntegerField()
    reboundRate = models.IntegerField()
    stealRate = models.IntegerField()
    blockRate = models.IntegerField()
    number = models.IntegerField(null=True)


class PositionPenalty(models.Model):
    code_origin_position = models.ForeignKey(Position, on_delete=models.CASCADE, related_name='origin_position')
    code_destination_position = models.ForeignKey(Position, on_delete=models.CASCADE,
                                                  related_name='destination_position')
    penalty = models.IntegerField()


class MoralPenalty(models.Model):
    class Meta:
        unique_together = (('moral', 'game_location'),)

    moral = models.IntegerField()
    game_location = models.CharField(null=False, max_length=10)
    penalty = models.IntegerField()


class Player(models.Model):
    name = models.CharField(max_length=25)
    age = models.IntegerField(null=False, default=99)
    team = models.CharField(max_length=25)
    signed = models.CharField(null=True, max_length=25)
    loaned = models.CharField(null=True, max_length=25)
    position = models.ForeignKey(Position, on_delete=models.CASCADE)
    onePointShoot = models.IntegerField()
    twoPointShoot = models.IntegerField()
    threePointShoot = models.IntegerField()
    dunk = models.IntegerField()
    rebound = models.IntegerField()
    block = models.IntegerField()
    passing = models.IntegerField()
    steal = models.IntegerField()
    price = models.FloatField(null=True)
    experience = models.IntegerField(null=True)
    motivated = models.BooleanField(null=False, default=True)
    enabled = models.BooleanField(null=False, default=True)
    international = models.BooleanField(null=False, default=False)
    moral = models.IntegerField(null=False, default=7)

    @property
    def favourite_number(self):
        return get_player_number(str(self.name))


class OffensiveConfiguration(models.Model):
    class Meta:
        unique_together = (('name', 'position'),)

    name = models.CharField(max_length=25, null=False)
    position = models.ForeignKey(Position, on_delete=models.CASCADE, null=False)
    interiorRate = models.IntegerField()
    midRate = models.IntegerField()
    exteriorRate = models.IntegerField()


class TacticPlay(models.Model):
    value = models.IntegerField()
    style = models.CharField(null=False, max_length=1)
    players = models.ManyToManyField(Player)


class Season(models.Model):
    code = models.CharField(primary_key=True, max_length=10, unique=True)
    year_beginning = models.IntegerField(null=False)
    year_ending = models.IntegerField(null=False)


class Competition(models.Model):
    code = models.CharField(primary_key=True, unique=True, max_length=25)
    name = models.CharField(max_length=50)


class PlayerSeason(models.Model):
    class Meta:
        unique_together = (('player', 'season', 'competition'),)

    player = models.ForeignKey(Player, on_delete=models.CASCADE)
    competition = models.ForeignKey(Competition, on_delete=models.CASCADE)
    season = models.ForeignKey(Season, on_delete=models.CASCADE)
    games = models.IntegerField(default=0)
    offensive_rebounds = models.IntegerField(default=0)
    defensive_rebounds = models.IntegerField(default=0)
    assists = models.IntegerField(default=0)
    steals = models.IntegerField(default=0)
    blocks = models.IntegerField(default=0)
    points = models.IntegerField(default=0)
