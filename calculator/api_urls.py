from django.conf.urls import url

from .api_players import *

urlpatterns = [
    url(r'^update_player$', update_player, name='update_player'),
    url(r'^add_player$', add_player, name='add_player'),
    url(r'^get_player_match_shoot', get_player_match_shoot, name='get_player_match_shoot'),
]
