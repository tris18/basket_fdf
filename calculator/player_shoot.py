from calculator.utils import range_creator


def get_interior_shoot_percentage(shoot: int, block: int) -> float:
    value = shoot - block

    if value < -12:
        return 0.36
    elif value < -6:
        return 0.38
    elif value < -4:
        return 0.40
    elif value < -2:
        return 0.41
    elif -2 <= value <= 2:
        return 0.42
    elif value > 32:
        return 0.58
    elif value > 30:
        return 0.57
    elif value > 28:
        return 0.56
    elif value > 26:
        return 0.55
    elif value > 24:
        return 0.54
    elif value > 22:
        return 0.53
    elif value > 20:
        return 0.52
    elif value > 18:
        return 0.51
    elif value > 16:
        return 0.50
    elif value > 14:
        return 0.49
    elif value > 12:
        return 0.48
    elif value > 10:
        return 0.47
    elif value > 8:
        return 0.46
    elif value > 6:
        return 0.45
    elif value > 4:
        return 0.44
    elif value > 2:
        return 0.43


def get_three_point_shoot_percentage(shoot: int, block: int) -> float:
    value = shoot - block

    if value < -12:
        return 0.11
    elif value < -6:
        return 0.13
    elif value < -4:
        return 0.15
    elif value < -2:
        return 0.16
    elif -2 <= value <= 2:
        return 0.17
    elif value > 32:
        return 0.33
    elif value > 30:
        return 0.32
    elif value > 28:
        return 0.31
    elif value > 26:
        return 0.30
    elif value > 24:
        return 0.29
    elif value > 22:
        return 0.28
    elif value > 20:
        return 0.27
    elif value > 18:
        return 0.26
    elif value > 16:
        return 0.25
    elif value > 14:
        return 0.24
    elif value > 12:
        return 0.23
    elif value > 10:
        return 0.22
    elif value > 8:
        return 0.21
    elif value > 6:
        return 0.20
    elif value > 4:
        return 0.19
    elif value > 2:
        return 0.18


def get_two_point_shoot_percentage(shoot: int, block: int) -> float:
    value = shoot - block
    if value < -12:
        return 0.21
    elif value < -6:
        return 0.23
    elif value < -4:
        return 0.25
    elif value < -2:
        return 0.26
    elif -2 <= value <= 2:
        return 0.27
    elif value > 32:
        return 0.43
    elif value > 30:
        return 0.42
    elif value > 28:
        return 0.41
    elif value > 26:
        return 0.40
    elif value > 24:
        return 0.39
    elif value > 22:
        return 0.38
    elif value > 20:
        return 0.37
    elif value > 18:
        return 0.36
    elif value > 16:
        return 0.35
    elif value > 14:
        return 0.34
    elif value > 12:
        return 0.33
    elif value > 10:
        return 0.32
    elif value > 8:
        return 0.31
    elif value > 6:
        return 0.30
    elif value > 4:
        return 0.29
    elif value > 2:
        return 0.28


def calculate_shoots_distribution(lineup_dict: dict):
    center = lineup_dict.get('center').get('data').get('points')
    power_forward = lineup_dict.get('power_forward').get('data').get('points')
    small_forward = lineup_dict.get('small_forward').get('data').get('points')
    shooting_guard = lineup_dict.get('shooting_guard').get('data').get('points')
    point_guard = lineup_dict.get('point_guard').get('data').get('points')

    res_center = lineup_dict.get('reserves').get('center').get('data').get('points')
    res_power_forward = lineup_dict.get('reserves').get('power_forward').get('data').get('points')
    res_small_forward = lineup_dict.get('reserves').get('small_forward').get('data').get('points')
    res_shooting_guard = lineup_dict.get('reserves').get('shooting_guard').get('data').get('points')
    res_point_guard = lineup_dict.get('reserves').get('point_guard').get('data').get('points')

    ratings = {center, power_forward, small_forward, shooting_guard, point_guard}
    res_ratings = {res_center, res_power_forward, res_small_forward, res_shooting_guard, res_point_guard}

    s_ratings = sorted(enumerate(ratings), key=lambda x: x[1], reverse=True)
    max_main = max(ratings)
    min_main = max(s_ratings[3][1], max_main - 2)

    shoot_ranges = range_creator(max_main, min_main, 6)

    s_ratings = sorted(enumerate(res_ratings), key=lambda x: x[1], reverse=True)
    max_res = max(res_ratings)
    min_res = max(s_ratings[3][1], max_res - 0.6)
    res_shoot_ranges = range_creator(max_res, min_res, 6)

    result = {}

    i = 0
    while i < 5:
        j = i + 1

        if shoot_ranges[i] <= center <= shoot_ranges[j]:
            result.update({'center': j})

        if shoot_ranges[i] <= power_forward <= shoot_ranges[j]:
            result.update({'power_forward': j})

        if shoot_ranges[i] <= small_forward <= shoot_ranges[j]:
            result.update({'small_forward': j})

        if shoot_ranges[i] <= shooting_guard <= shoot_ranges[j]:
            result.update({'shooting_guard': j})

        if shoot_ranges[i] <= point_guard <= shoot_ranges[j]:
            result.update({'point_guard': j})

        if res_shoot_ranges[i] <= res_center <= res_shoot_ranges[j]:
            result.update({'res_center': j})

        if res_shoot_ranges[i] <= res_power_forward <= res_shoot_ranges[j]:
            result.update({'res_power_forward': j})

        if res_shoot_ranges[i] <= res_small_forward <= res_shoot_ranges[j]:
            result.update({'res_small_forward': j})

        if res_shoot_ranges[i] <= res_shooting_guard <= res_shoot_ranges[j]:
            result.update({'res_shooting_guard': j})

        if res_shoot_ranges[i] <= res_point_guard <= res_shoot_ranges[j]:
            result.update({'res_point_guard': j})

        i += 1

    if center <= min_main:
        result.update({'center': 1})

    if power_forward <= min_main:
        result.update({'power_forward': 1})

    if small_forward <= min_main:
        result.update({'small_forward': 1})

    if shooting_guard <= min_main:
        result.update({'shooting_guard': 1})

    if point_guard <= min_main:
        result.update({'point_guard': 1})

    if res_center <= min_res:
        result.update({'res_center': 1})

    if res_power_forward <= min_res:
        result.update({'res_power_forward': 1})

    if res_small_forward <= min_res:
        result.update({'res_small_forward': 1})

    if res_shooting_guard <= min_res:
        result.update({'res_shooting_guard': 1})

    if res_point_guard <= min_res:
        result.update({'res_point_guard': 1})
    return result


def calculate_shoots_distribution_simple(lineup_dict: dict):
    center = lineup_dict.get('center').get('data').get('real_rating')
    power_forward = lineup_dict.get('power_forward').get('data').get('real_rating')
    small_forward = lineup_dict.get('small_forward').get('data').get('real_rating')
    shooting_guard = lineup_dict.get('shooting_guard').get('data').get('real_rating')
    point_guard = lineup_dict.get('point_guard').get('data').get('real_rating')

    center_rating = 1
    power_forward_rating = 1
    small_forward_rating = 1
    shooting_guard_rating = 1
    point_guard_rating = 1

    i = 0
    while i < 8:

        ratings = {0 if center_rating == 5 else center, 0 if power_forward_rating == 5 else power_forward,
                   0 if small_forward_rating == 5 else small_forward,
                   0 if shooting_guard_rating == 5 else shooting_guard,
                   0 if point_guard_rating == 5 else point_guard}
        """

        ratings = {center, power_forward, small_forward, shooting_guard, point_guard}
        """
        if max(ratings) == center:
            center_rating += 1
            center -= 1
        elif max(ratings) == power_forward:
            power_forward_rating += 1
            power_forward -= 1
        elif max(ratings) == small_forward:
            small_forward_rating += 1
            small_forward -= 1
        elif max(ratings) == shooting_guard:
            shooting_guard_rating += 1
            shooting_guard -= 1
        elif max(ratings) == point_guard:
            point_guard_rating += 1
            point_guard -= 1
        i += 1

    result = {
        'center': center_rating,
        'power_forward': power_forward_rating,
        'small_forward': small_forward_rating,
        'shooting_guard': shooting_guard_rating,
        'point_guard': point_guard_rating
    }

    return result
